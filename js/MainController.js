angular
    .module('app')
    .controller('MainController', MainController);

MainController.$inject = ['$interval'];

function MainController($interval) {

    let vm = this;

    countdown();

    vm.currentCount = '';

    vm.drawSiteOne = drawSiteOne;
    vm.drawSiteTwo = drawSiteTwo;
    vm.drawSiteThree = drawSiteThree;
    vm.drawSiteFour = drawSiteFour;

    vm.isOneDone = false;
    vm.isTwoDone = false;
    vm.isThreeDone = false;
    vm.isFourDone = false;

    vm.invokeOne = function () {
        let check = !vm.isOneDone;
        vm.isOneDone = check;
        return vm.isOneDone;
    };

    vm.invokeTwo = function () {
        let check = !vm.isTwoDone;
        vm.isTwoDone = check;
        return vm.isTwoDone;
    };

    vm.invokeThree = function () {
        let check = !vm.isThreeDone;
        vm.isThreeDone = check;
        return vm.isThreeDone;
    };

    vm.invokeFour = function () {
        let check = !vm.isFourDone;
        vm.isFourDone = check;
        return vm.isFourDone;
    };

    class SpaceCanvas {
        constructor() {
            this.canvas = document.getElementById('myCanvas');
            this.context = this.canvas.getContext('2d');

            this.centerX = this.canvas.width / 2;
            this.centerY = this.canvas.height / 2;
            this.radius = 20;
        }
    }


    function drawSiteOne(isDone) {
        let canv = new SpaceCanvas();
        let color = 'yellow';
        let radius = 20;
        let context = canv.context;
        let centerX = canv.centerX;
        let centerY = canv.centerY;

        isDone ? color = 'green': color = 'red';

        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 5;
        context.strokeStyle = '#003300';
        context.stroke();
    }



    function drawSiteTwo(isDone) {
        let canv = new SpaceCanvas();
        let color = 'yellow';
        let radius = 20;
        let context = canv.context;
        let centerX = canv.centerX;
        let centerY = canv.centerY;

        isDone ? color = 'green': color = 'red';

        context.beginPath();
        context.arc(centerX/ 2, centerY/ 2, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 5;
        context.strokeStyle = '#003300';
        context.stroke();
    }

    function drawSiteThree(isDone) {
        let canv = new SpaceCanvas();
        let color = 'yellow';
        let radius = 20;
        let context = canv.context;
        let centerX = canv.centerX / 2;
        let centerY = canv.centerY / 2;

        isDone ? color = 'green': color = 'red';

        centerX = 3 * canv.centerX;
        centerY = canv.centerY;

        context.beginPath();
        context.arc(centerX/ 2, centerY/ 2, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 5;
        context.strokeStyle = '#003300';
        context.stroke();
    }

    function drawSiteFour(isDone) {
        let canv = new SpaceCanvas();
        let color = 'yellow';
        let radius = 20;
        let context = canv.context;
        let centerX = canv.centerX / 2;
        let centerY = canv.centerY / 2;


        centerX = 2.5 * canv.centerX;
        centerY = (canv.centerX) / 4;

        isDone ? color = 'green': color = 'red';

        context.beginPath();
        context.arc(centerX/ 2, centerY/ 2, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 5;
        context.strokeStyle = '#003300';
        context.stroke();
    }

    drawSiteOne(false);
    drawSiteTwo(false);
    drawSiteThree(false);
    drawSiteFour(false);

    function countdown() {
        // Set the date we're counting down to
        let countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();

        // Update the count down every 1 second
        let x = $interval(function() {

            // Get todays date and time
            let now = new Date().getTime();

            // Find the distance between now an the count down date
            let distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"

            let display = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            console.log('the display is');
            console.log(display);
            vm.currentCount = display;

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                vm.currentCount = "EXPIRED";
            }
        }, 1000);
    }
}